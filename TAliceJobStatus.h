#ifndef ROOT_TAliceJobStatus
#define ROOT_TAliceJobStatus

#include "TGridJobStatus.h"

class TAliceJobStatus : public TGridJobStatus {
 public:
    virtual const char *GetJdlKey(const char *key) = 0;
    virtual const char *GetKey(const char *key) = 0;
};

#endif
